﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Bellman_Ford
{
    public class Edge
    {
        public int From, To;
        public int Cost;
    }

    public class Graph
    {
        public List<Edge> Edges;
        public int From;
        public int To;
    }

    class Program
    {
        static void Main(string[] args)
        {
            var graph = ReadGraph();
            var max = GetMaxPathCost(graph.Edges, graph.From, graph.To);
            var cost = max.Item1;
            var path = max.Item2.Select(x => x + 1).Reverse();
            var result = cost == int.MinValue ? "N" : "Y";
            var answer = result == "N" ? "N" : $"{result}\n{String.Join("-", path)}\n{cost}";
            Console.WriteLine(answer);
            File.WriteAllLines(@"C:\Coding\Education\Сombinatorial Algorithms\Bellman–Ford algorithm\out.txt", answer.Split('\n'));
        }

        private static Graph ReadGraph()
        {
            var edges = new List<Edge>();
            var lines = File.ReadAllLines(@"C:\Coding\Education\Сombinatorial Algorithms\Bellman–Ford algorithm\in.txt");
            var n = int.Parse(lines[0]);
            for (var i = 1; i < n + 1; i++)
            {
                var costs = lines[i].Split(' ').Select(int.Parse).ToArray();
                for (var j = 0; j < costs.Length; j++)
                    if (costs[j] != -32768)
                        edges.Add(new Edge { From = i - 1, To = j, Cost = costs[j]});
            }
            var from = int.Parse(lines[lines.Length - 2]) - 1;
            var to = int.Parse(lines[lines.Length - 1]) - 1;
            return new Graph { From = from, To = to, Edges = edges };
        }

        public static Tuple<int, Stack<int>> GetMaxPathCost(List<Edge> edges, int startNode, int finalNode)
        {
            var maxNodeIndex = edges.SelectMany(e => new[] { e.From, e.To }).Concat(new[] { startNode, finalNode }).Max();
            var opt = Enumerable.Repeat(int.MinValue, maxNodeIndex + 1).ToArray();
            opt[startNode] = 1;
            var stack = new Stack<int>();
            stack.Push(startNode);
            for (var pathSize = 1; pathSize <= maxNodeIndex; pathSize++)
            {
                foreach (var edge in edges)
                {
                    if (opt[edge.From] == int.MinValue) continue;
                    if (opt[edge.From] * edge.Cost >= opt[edge.To] && !stack.Contains(edge.From))
                        stack.Push(edge.From);
                    opt[edge.To] = Math.Max(opt[edge.To], opt[edge.From] * edge.Cost);
                }
            }
            if (!stack.Contains(finalNode))
                stack.Push(finalNode);
            while (stack.Peek() != finalNode)
                stack.Pop();
            return new Tuple<int, Stack<int>>(opt[finalNode], stack);
        }
    }
}
